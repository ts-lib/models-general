<?php

namespace TsLib\ModelsGeneral;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
	protected $guarded = [];
}