<?php

namespace TsLib\ModelsGeneral;

class UsersPermissions extends BaseModel
{
    protected $table = "user_has_permissions";
    public $timestamps = false;

    protected $fillable = [
        "permission_id",
        "user_id"
    ];
}
