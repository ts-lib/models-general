<?php

namespace TsLib\ModelsGeneral;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Laravel\Lumen\Auth\Authorizable;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "id",
        "code",
        "name",
        "email",
        "password",
        "active",
        "access",
        "location_id",
        "type",
        "remember_token",
        "password_link",
        "clave_access"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function scopeContactos($query)
    {
        return $query->where('type','contact');
    }

    public function scopeActivo($query)
    {
        return $query->where('active','1');
    }

    public function Customer()
    {
        if(class_exists(TsLib\ModelsSales\Customer::class))
            return $this->hasOne('TsLib\ModelsSales\Customer');
        else
            return null;
    }

    public function Contact()
    {
        if(class_exists(TsLib\ModelsSales\Contact::class))
            return $this->hasOne('TsLib\ModelsSales\Contact');
        else
            return null;
    }

   public function getLocationNameAttribute()
    {
       $parent = [
            '1', 'CDMX',
            '2', '2',
            '3', '3',
            '4', '4',
            '5', '5',
            '6', '6',
            '7'=> 'CDMX',
            '8'=> 'GDL',
            '9'=> 'MER',
            '10'=> 'PUE',
            '11'=> 'QRO',
            '12'=> 'VER',
            '13'=> 'CDMX',
            '14'=> 'CDMX',
            '16'=> 'CDMX',
            '17'=> 'CDMX',
            '18'=> 'CDMX',
            '19'=> '5',
            '20'=> '5',
            '22'=> '5',
            '23'=> 'CDMX',
            '24'=> 'CDMX',
            '26'=> '2',
            '28'=> '2',
            '31'=> '2',
            '35'=> '4',
            '38'=> '4',
            '40'=> '3',
            '41'=> '3',
            '43'=> '4',
            '44'=> '3',
            '49'=> '3',
            '50'=> '6',
            '52'=> '6',
            '55'=> '6',
            '69'=> 'CDMX',
            '70'=> '3',
            '71'=> '4',
            '72'=> '2',
            '73'=> '6',
            '74'=> '5',
            '75'=> 'CDMX',
            '80'=> '80',
            '82'=> '80',
            '83'=> 'MTY',
            '84'=> '80',
            '85'=> 'CDMX',
            '86'=> 'CDMX',
            '87'=> 'CDMX',
            '88'=> 'CDMX'
        ];
        return isset($parent[$this->location_id]) ? $parent[$this->location_id] : 'CDMX';
    }

    public function getPortalTokenAttribute()
    {
        return base64_encode($this->code.$this->id);
    }
}
